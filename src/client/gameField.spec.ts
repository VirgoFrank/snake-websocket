import { drawField } from './gameField';

describe('gameField', () => {
  beforeEach(() => {
    document.body.innerHTML = '';
  });

  it('drawField 2x3', () => {
    const $game = document.createElement('div');
    $game.setAttribute('id', 'game');
    document.body.appendChild($game);

    expect($game.childNodes).toHaveLength(0);
    drawField(2, 3);
    expect($game.childNodes).toHaveLength(6);
  });

  it('drawField 5x5', () => {
    const $game = document.createElement('div');
    $game.setAttribute('id', 'game');
    document.body.appendChild($game);

    expect($game.childNodes).toHaveLength(0);
    drawField(5, 5);
    expect($game.childNodes).toHaveLength(25);
  });
});
